/* This file is part of Codecraft, and IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2023 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecraft is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

/* includes */
#include <strings.h>

#include <exec/memory.h>
#include <intuition/intuition.h>
#include <intuition/gadgetclass.h>
#include <intuition/icclass.h>

#include <clib/alib_protos.h>

#include <proto/intuition.h>
#include <proto/gadtools.h>
#include <proto/intuition.h>
#include <proto/graphics.h>
#include <proto/utility.h>
#include <proto/locale.h>
#include <proto/dos.h>
#include <proto/icon.h>
#include <proto/exec.h>
#include <proto/window.h>
#include <proto/layout.h>
#include <proto/listbrowser.h>
#include <proto/button.h>
#include <proto/clicktab.h>
#include <proto/label.h>
#include <proto/string.h>

#include "codecraft.h"
#include <tools/textedit/extension.h>

#include "iccallback.h"

enum gadids
{
  GID_OK = 1
 ,GID_CANCEL
 ,GID_SEARCHSTRING
 , MAXGADGETS
};

extern struct Codecraft myCodecraft;

static Object *gadgets[MAXGADGETS];

static struct Window *searchFilesWindow = NULL;
static Object *searchFilesWindowObj = NULL;
static Object *mainlayout;
static BOOL	firstActivation;

static struct Requester BlockingReq;

extern struct Library *UtilityBase;
extern struct Catalog *Catalog;
extern struct Locale *Locale;

#define CATCOMP_NUMBERS;
#include "strings.h"
const UBYTE *GetStr(LONG stringNum);

void closeSearchFiles(struct Codecraft *ads);
void createSearchFilesBrowser(struct Codecraft *ads);
void StartSearch(struct Codecraft *ads, STRPTR str);

void openSearchFilesRequester(struct Codecraft *ads)
{
	ULONG sigmask;
	
	searchFilesWindowObj = NewObject(WINDOW_GetClass(), NULL,
		WA_Activate, TRUE,
		WA_DragBar, TRUE,
		WA_DepthGadget, TRUE,
		WA_SizeGadget, TRUE,
		WA_Title, GetStr(MSG_SEARCHFILES_TITLE),
		WA_Left, ads->window->LeftEdge + 50,
		WA_Top, ads->window->TopEdge + 30,
		WA_Width, 500,
		WA_Height, 180,
		WA_AutoAdjust, TRUE,
		WINDOW_GadgetHelp, TRUE,
		WA_IDCMP, IDCMP_CLOSEWINDOW|IDCMP_GADGETUP,
		WINDOW_Layout, mainlayout = NewObject(LAYOUT_GetClass(), NULL,
			LAYOUT_DeferLayout, TRUE,
			LAYOUT_Orientation, LAYOUT_ORIENT_VERT,
			LAYOUT_SpaceInner, TRUE,
			LAYOUT_SpaceOuter, TRUE,
			LAYOUT_AddChild, gadgets[GID_SEARCHSTRING] = NewObject(STRING_GetClass(), NULL,
				GA_ID, GID_SEARCHSTRING,
				GA_RelVerify, TRUE,
				GA_TabCycle, TRUE,
				TAG_END),
			CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, GetStr(MSG_SEARCHFILES_STR_LBL),
				TAG_END),
			LAYOUT_AddChild, NewObject(LAYOUT_GetClass(), NULL,
				TAG_DONE),
			CHILD_WeightedHeight, 100,
			LAYOUT_AddChild, NewObject(LAYOUT_GetClass(), NULL,
				LAYOUT_EvenSize, TRUE,
				LAYOUT_AddChild, NewObject(NULL, "button.gadget",
					GA_ID, GID_OK,
					GA_RelVerify, TRUE,
					GA_Text, GetStr(MSG_OK_GAD),
					BUTTON_TextPadding, TRUE,
					GA_TabCycle, TRUE,
					TAG_END),
				CHILD_WeightedWidth, 1,
				LAYOUT_AddChild, NewObject(LABEL_GetClass(), NULL, LABEL_Text, "",
					TAG_END),
				CHILD_WeightedWidth, 100,
				LAYOUT_AddChild, NewObject(NULL, "button.gadget",
					GA_ID, GID_CANCEL,
					GA_RelVerify, TRUE,
					GA_Text, GetStr(MSG_CANCEL_GAD),
					BUTTON_TextPadding, TRUE,
					GA_TabCycle, TRUE,
					TAG_END),
				CHILD_WeightedWidth, 1,
				TAG_DONE),
			CHILD_WeightedHeight, 0,
			TAG_DONE),
		TAG_DONE);

	InitRequester(&BlockingReq);
	Request(&BlockingReq, ads->window);
	SetWindowPointer(ads->window, WA_BusyPointer, TRUE, TAG_DONE);

	firstActivation = TRUE;
	searchFilesWindow = (struct Window *)DoMethod(searchFilesWindowObj, WM_OPEN, NULL);

	if (!searchFilesWindowObj)
		closeSearchFiles(ads);

	GetAttr(WINDOW_SigMask, searchFilesWindowObj, &sigmask);

	ads->ext->sigMask |= sigmask;
}

void closeSearchFiles(struct Codecraft *ads)
{
	if (searchFilesWindowObj)
	{
		ULONG sigmask;

		GetAttr(WINDOW_SigMask, searchFilesWindowObj, &sigmask);
		ads->ext->sigMask &= ~sigmask;
		DisposeObject(searchFilesWindowObj);
		searchFilesWindowObj  = NULL;
		searchFilesWindow = NULL;

		SetWindowPointer(ads->window, TAG_DONE);
		EndRequest(&BlockingReq, ads->window);
	}
}

void searchFilesEventHandler(struct Codecraft *ads)
{
	ULONG result;
	ULONG code;

	if (!searchFilesWindowObj)
		return;

	while (result = DoMethod(searchFilesWindowObj, WM_HANDLEINPUT, &code))
	{
		switch (result & WMHI_CLASSMASK)
		{
		case WMHI_ACTIVE:
			if (firstActivation)
				ActivateLayoutGadget(mainlayout, searchFilesWindow, NULL, gadgets[GID_SEARCHSTRING]);
			firstActivation = FALSE;
			break;
			
		case WMHI_GADGETUP:
			switch(result & WMHI_GADGETMASK)
			{
				case GID_SEARCHSTRING:
					// INTENTIONAL FALL THROUGH					
				case GID_OK:
				{
					if (! ads->searchFilesBrowser)
						createSearchFilesBrowser(ads);
					
					if (ads->searchFilesBrowser)
					{
						STRPTR str;
						
					    SetGadgetAttrs((struct Gadget *)ads->searchFilesBrowser, ads->window, NULL,
					    	LISTBROWSER_Labels, (ULONG) NULL,
					    	TAG_END);
						
						GetAttr(STRINGA_TextVal, gadgets[GID_SEARCHSTRING], (ULONG *)&str);
						StartSearch(ads, str);
					}
				}
				/* intentional fall through */
				case GID_CANCEL:
					closeSearchFiles(ads);
					break;
			}
			break;
		}
	}
}

ULONG searchHandler(struct TagItem *taglist)
{
	struct Codecraft *ads = &myCodecraft;
	struct Node *node;
	ULONG relevant;
	struct TagItem *tstate, *ti; /* grab some temp variables off of the stack. */
	ti = taglist;
	tstate = ti;

	while (ti = NextTagItem(&tstate)) /* Step through all of the attribute/value */
	{
		switch (ti->ti_Tag)
		{
			case LISTBROWSER_SelectedNode:
				node = (struct Node *)ti->ti_Data;
				GetAttr(LISTBROWSER_RelEvent, ads->searchFilesBrowser, &relevant);
				if (relevant == LBRE_DOUBLECLICK)
				{
					ULONG *blockNum = 0;
					
					GetListBrowserNodeAttrs(node,
						LBNA_Column, 1,
						LBNCA_Integer, (ULONG *)&blockNum,
						TAG_DONE);
					
					if (node->ln_Name)
					{
						LONG len = strlen(node->ln_Name);
						struct RevealLocationMsg *rlm;
						rlm = (struct RevealLocationMsg *)AllocVec(sizeof(struct RevealLocationMsg) + len+1, MEMF_CLEAR);
						
						if (rlm)
						{
							strcpy(rlm->filename, node->ln_Name);
							rlm->blockNum = *blockNum;
							rlm->type = 0;
							PutMsg(ads->revealPort, (struct Message *)rlm);
						}
					}
				}
				break;
		}
	}
	return TRUE;
}

Object *icobject;

void createSearchFilesBrowser(struct Codecraft *ads)
{
	struct Node *node;
	
    NewList(&ads->searchFilesBrowserList);
    
	icobject = NewObject(ICCallbackClass, NULL,
			TAG_USER, searchHandler,
			TAG_DONE);

    ads->searchFilesBrowser = NewObject(LISTBROWSER_GetClass(), NULL,
    	GA_ID, GLOBALGID_SEARCHBROWSER,
    	GA_RelVerify, TRUE,
    	LISTBROWSER_ShowSelected, TRUE,
    	LISTBROWSER_MultiSelect, FALSE,
    	LISTBROWSER_Separators, FALSE,
    	LISTBROWSER_Hierarchical, TRUE,
    	LISTBROWSER_StayActive, TRUE,
    	ICA_TARGET, icobject,
    	TAG_DONE);

	node = AllocClickTabNode(
		TNA_Text, GetStr(MSG_UTILTAB_SEARCHRESULTS),
		TNA_CloseGadget, TRUE,
		TAG_END);
	
    SetGadgetAttrs((struct Gadget *)ads->utilityClickTab, ads->window, NULL,
    	CLICKTAB_Labels, ~0,
    	TAG_END);

	AddTail(&ads->utilityTabs, node);
    
    ads->SearchTab = node;
    
    SetGadgetAttrs((struct Gadget *)ads->utilityPageGroup, ads->window, NULL,
    	PAGE_Add, ads->searchFilesBrowser,
    	TAG_END);
    	
    SetGadgetAttrs((struct Gadget *)ads->utilityClickTab, ads->window, NULL,
    	CLICKTAB_Labels, &ads->utilityTabs,
    	TAG_END);
}

STRPTR prevPath;

void addSearchResult(struct Codecraft *ads, STRPTR path, STRPTR line, LONG blockNum)
{
	struct Node *node;
	
    SetGadgetAttrs((struct Gadget *)ads->searchFilesBrowser, ads->window, NULL,
    	LISTBROWSER_Labels, (ULONG) ~0,
    	TAG_END);

	if (ads->searchFilesBrowserList.lh_Head->ln_Succ == NULL)
	{
		SetGadgetAttrs(	(struct Gadget *) ads->utilityClickTab, ads->window, NULL,
			CLICKTAB_CurrentNode, ads->SearchTab,
			TAG_DONE);
	}
	
	if (Stricmp(path, prevPath) != 0)
	{
		node = AllocListBrowserNode(1,
			LBNA_Generation, 1,
			LBNA_Flags, LBFLG_HASCHILDREN|LBFLG_SHOWCHILDREN,
			LBNA_Column, 0,
			LBNCA_CopyText, TRUE,
			LBNCA_Text, path,
			TAG_DONE);

		AddTail(&ads->searchFilesBrowserList, node);

		GetListBrowserNodeAttrs(node,
			LBNCA_Text, (ULONG *)&prevPath,
			NULL
		);
	}

	node = AllocListBrowserNode(2,
		LBNA_Generation, 2,
		LBNA_Column, 0,
		LBNCA_CopyText, TRUE,
		LBNCA_Text, line,
		LBNA_Column, 1,
		LBNCA_CopyInteger, TRUE,
		LBNCA_Integer, &blockNum,		
		TAG_DONE);

	node->ln_Name = prevPath;
	
	AddTail(&ads->searchFilesBrowserList, node);
	
    SetGadgetAttrs((struct Gadget *)ads->searchFilesBrowser, ads->window, NULL,
    	LISTBROWSER_Labels, &ads->searchFilesBrowserList,
    	TAG_END);
}

LONG detectBinary(STRPTR buffer)
{
	while (*buffer)
	{
		if ((*buffer < 32) && (*buffer != 13) && (*buffer != 10))
			return TRUE;
		
		buffer++;
	}
	return FALSE;
}

LONG *generateKmpTable(STRPTR pattern)
{
	ULONG pattern_len = strlen(pattern);
    LONG i = 0, j = -1;
    LONG *table = (LONG *)AllocVec((pattern_len + 1) * sizeof(LONG), MEMF_ANY);
    
    if (! table)
	    return NULL;
	    
    table[0] = -1;
    
    while (i < pattern_len) {
        while (j >= 0 && pattern[i] != pattern[j]) {
            j = table[j];
        }
        i++;
        j++;
        table[i] = j;
    }
    
    return table;
}

void searchInFile(struct Codecraft *ads, STRPTR str, LONG *table, STRPTR path)
{
	BPTR file;
	TEXT buffer[1024];
	STRPTR lineBuffer = buffer + 7;
	LONG blockNumber = 0;
	LONG lineLen;
	ULONG stringLen = strlen(str);
	
	file = Open(path, MODE_OLDFILE);
	
	if (!file)
		return;
	
	while (FGets(file, lineBuffer, 1024-7))
	{
		LONG next_match_pos = 0;
		ULONG pos;
		
		blockNumber++;
		
		lineLen = strlen(lineBuffer);
		if (lineLen >1000)
			break;

		if (blockNumber < 6 && detectBinary(lineBuffer))
			break;
			
		lineBuffer[lineLen-1] = 0;
		
		for (pos = 0; pos < lineLen; pos++)
		{
			while (next_match_pos >= 0 && str[next_match_pos] != lineBuffer[pos])
			{
				next_match_pos = table[next_match_pos];
			}

			next_match_pos++;

			if (next_match_pos == stringLen)
			{
				LONG leadBlanks = 0;
				while (IsSpace(Locale, lineBuffer[leadBlanks]))
					leadBlanks++;
				SNPrintf(buffer, 1024, "%ld: %s", blockNumber, lineBuffer+leadBlanks);
				addSearchResult(ads, path, buffer, blockNumber-1);
				break;
			}
		}    	
	}
	
	
	Close(file);
}

void searchRecurse(struct Codecraft *ads, STRPTR str, LONG *table, BPTR dirLock, STRPTR path, LONG pathLen)
{
	struct FileInfoBlock *fib;	
	
	fib = (struct FileInfoBlock *)AllocDosObject(DOS_FIB, TAG_DONE);
	
	Examine(dirLock, fib);
	
	if (pathLen)
	{
		path[pathLen] = '/';
		pathLen++;
	}
	
	while (ExNext(dirLock, fib))
	{
		if (fib->fib_FileName[0] == '.')
			continue;
			
		strcpy(path + pathLen, fib->fib_FileName);
		
		if (fib->fib_DirEntryType < 0)
		{
			BPTR oldDir;
			
			oldDir = CurrentDir(dirLock);
			searchInFile(ads, str, table, path);
			CurrentDir(oldDir);
		}
		else
		{
			BPTR subLock;
			BPTR oldDir;
			
			oldDir = CurrentDir(dirLock);
			subLock = Lock(fib->fib_FileName, SHARED_LOCK);
			CurrentDir(oldDir);
			
			if (subLock)
				searchRecurse(ads, str, table, subLock, path, pathLen+strlen(fib->fib_FileName));
			
			UnLock(subLock);
		}	
	}
	
	FreeDosObject(DOS_FIB, fib);
}

void StartSearch(struct Codecraft *ads, STRPTR str)
{
	TEXT path[2048];
	LONG *table;
	struct Node *node;
		
	prevPath = "";
	path[0] = 0;


	// First clear out old results
    SetGadgetAttrs((struct Gadget *)ads->searchFilesBrowser, ads->window, NULL,
    	LISTBROWSER_Labels, NULL,
    	TAG_END);
	while (node = RemTail(&ads->searchFilesBrowserList))
		FreeListBrowserNode(node);
    SetGadgetAttrs((struct Gadget *)ads->searchFilesBrowser, ads->window, NULL,
    	LISTBROWSER_Labels, &ads->searchFilesBrowserList,
    	TAG_END);


	table = generateKmpTable(str);
	
	if (table)
		searchRecurse(ads, str, table, ads->projectDirLock, path, 0);
		
	FreeVec(table);
}

void CloseSearch(struct Codecraft *ads)
{
	struct Node *node;

    SetGadgetAttrs((struct Gadget *)ads->utilityClickTab, ads->window, NULL,
    	CLICKTAB_Labels, ~0,
    	TAG_END);

	Remove(ads->SearchTab);
    
    SetGadgetAttrs((struct Gadget *)ads->utilityPageGroup, ads->window, NULL,
    	PAGE_Remove, ads->searchFilesBrowser,
    	TAG_END);
    	
    SetGadgetAttrs((struct Gadget *)ads->utilityClickTab, ads->window, NULL,
    	CLICKTAB_Labels, &ads->utilityTabs,
    	TAG_END);
		
	ads->searchFilesBrowser = NULL;

	FreeClickTabNode(ads->SearchTab);
	ads->SearchTab = NULL;

	DisposeObject(icobject);
	icobject = NULL;
	
	while (node = RemTail(&ads->searchFilesBrowserList))
		FreeListBrowserNode(node);
}
