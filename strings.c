/* includes */
#include <clib/alib_protos.h>

#include <proto/exec.h>
#include <proto/locale.h>

#undef CATCOMP_CODE
#undef LOCALESTR_CODECRAFT_H
#define CATCOMP_BLOCK

#include "strings.h"

extern APTR Catalog;

struct CatCompBlockType
{
	LONG	ccb_ID;
	UWORD	ccb_StringSize;
};

const UBYTE *GetStr(LONG stringNum)
{
	const struct CatCompBlockType * ccb = (APTR)CatCompBlock;
	const struct CatCompBlockType * ccb_stop = (APTR)&((BYTE *)ccb)[sizeof(CatCompBlock)];

	STRPTR builtin = NULL;
	STRPTR result;

	while(ccb < ccb_stop && ccb->ccb_StringSize > 0)
	{
		if(ccb->ccb_ID == stringNum)
		{
			builtin = (STRPTR)&ccb[1];
			break;
		}

		ccb = (struct CatCompBlockType *)&((BYTE *)ccb)[sizeof(*ccb) + ccb->ccb_StringSize];
	}

	if(LocaleBase != NULL)
		result = GetCatalogStr(Catalog, stringNum, builtin);
	else
		result = builtin;

	return result;
}

