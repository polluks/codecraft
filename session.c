/* This file is part of Codecraft, and IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecrafr is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdlib.h>

#include <intuition/classusr.h>
#include <dos/dos.h>
#include <dos/exall.h>
#include <dos/dostags.h>

#define __CLIB_PRAGMA_LIBCALL
#include <clib/alib_protos.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/graphics.h>
#include <proto/utility.h>
#include <proto/locale.h>
#include <proto/dos.h>
#include <proto/listbrowser.h>

#include <tools/textedit/extension.h>
#include "codecraft.h"
#include "breakpoints.h"

extern struct Locale *Locale;
extern struct Catalog *Catalog;

extern struct Codecraft myCodecraft;

static void writeDocToSession(struct Codecraft *ads, BPTR file, struct CodecraftDoc *adsDoc)
{
	STRPTR s;
	struct BreakPoint *breakPoint;

	FPuts(file, adsDoc->fullPath);
	FPutC(file, '\n');
	if (adsDoc->document)
	{
		s = ads->tei->executeRexxCommand(adsDoc->document, "GETCURSOR LINE");
		FPuts(file, s);
		FreeVec(s);
		FPutC(file, '\n');
		s = ads->tei->executeRexxCommand(adsDoc->document, "GETCURSOR COLUMN");
		FPuts(file, s);
		FreeVec(s);
		FPutC(file, '\n');
	}
	else
		FPuts(file, "-1\n-1\n");

	breakPoint = (struct BreakPoint *)adsDoc->breakPointList.mlh_Head;
	while (breakPoint->node.mln_Succ)
	{
		FPrintf(file, "%ld\n", breakPoint->blockNum);
		breakPoint  = (struct BreakPoint *)breakPoint->node.mln_Succ;
	}
	FPuts(file, "EndOfBreakPoints\n");
	
	if (ads->currentAdsDocument == adsDoc)
		FPuts(file, "IsCurrentDocument\n");

	FPuts(file, "EndOfPersistedFile\n");
}

struct CodecraftDoc *extractBreakpointDocument(struct Codecraft *ads, APTR document)
{
	struct CodecraftDoc *adsDoc = (struct CodecraftDoc *)ads->breakpointDocList.mlh_Head;
	UBYTE *docFullPath;

	if (!adsDoc->node.mln_Succ)
		return NULL;

	docFullPath = ads->tei->executeRexxCommand(document, "GETATTR PROJECT FILENAME");

	while (adsDoc->node.mln_Succ)
	{
		if (Stricmp(adsDoc->fullPath, docFullPath) == 0)
		{
			FreeVec(docFullPath);
			Remove((struct Node *)adsDoc);
			return adsDoc;
		}
		adsDoc = (struct CodecraftDoc *)adsDoc->node.mln_Succ;
	}
	FreeVec(docFullPath);
	return NULL;
}

void writeExpandedNodes(struct Codecraft *ads, BPTR file, STRPTR buffer)
{
	struct Node *node;
	LONG curGeneration = 0;

    FPuts(file, "BeginExpandedNodes\n");
    node = ads->projectTreeList.lh_Head;
    
    if (! node->ln_Succ)
	    return;
	    
    node = node->ln_Succ; // skip the node with the project name
    *buffer = 0;
	
	while (node->ln_Succ)
	{
		STRPTR nodeName;
		ULONG flags;
		LONG generation;
		
		GetListBrowserNodeAttrs(node,
			LBNA_Generation, &generation,
			LBNA_Flags, &flags,
			LBNA_Column, 0,
			LBNCA_Text, (ULONG *)&nodeName,
			NULL
		);

		while (curGeneration >= generation)
		{
			*PathPart(buffer) = 0;
			curGeneration--;
		}

		if (flags & LBFLG_HASCHILDREN)
		{
			if (generation > 1)
				Strncat(buffer, "/", MAXPATHLEN);
			Strncat(buffer, nodeName, MAXPATHLEN);
			curGeneration++;
			if (flags & LBFLG_SHOWCHILDREN)
			{
			    FPuts(file, buffer);
				FPutC(file, '\n');

			}
		}
			
		node = node->ln_Succ;
	}
    FPuts(file, "EndOfExpandedNodes\n");	
}

void saveSession()
{
    struct Codecraft *ads = &myCodecraft;
    BPTR file;
    BPTR olddir;
	TEXT buffer[MAXPATHLEN];
    struct CodecraftDoc *adsDoc = (struct CodecraftDoc *)ads->documentList.mlh_Head;

	if (!ads->projectDirLock)
		return;

	olddir = CurrentDir(ads->projectDirLock);
	SNPrintf(buffer, sizeof(buffer), "%s.session",ads->project.name);
	file = Open(buffer, MODE_NEWFILE);
	CurrentDir(olddir);

    if (!file)
        return;

    FPuts(file, "Codecraft session file\n");
    while (adsDoc->node.mln_Succ)
    {
		if (FilePart(adsDoc->fullPath) != adsDoc->fullPath)
			writeDocToSession(ads, file, adsDoc);
		adsDoc = (struct CodecraftDoc *)adsDoc->node.mln_Succ;
	}
	adsDoc = (struct CodecraftDoc *)ads->breakpointDocList.mlh_Head;
	while (adsDoc->node.mln_Succ)
	{
		writeDocToSession(ads, file, adsDoc);
		adsDoc = (struct CodecraftDoc *)adsDoc->node.mln_Succ;
	}
	
	writeExpandedNodes(ads, file, buffer);
	
	Close(file);
	SetProtection("session", FIBF_EXECUTE);
}

void expandNodes(struct Codecraft *ads, BPTR file, STRPTR buffer)
{
	while (FGets(file, buffer, MAXPATHLEN))
	{
	    if (Stricmp(buffer, "EndOfExpandedNodes\n")== 0)
		    return;
		expandNode(ads->projectTreeList.lh_Head->ln_Succ, buffer, TRUE);
	}
}


void loadSession()
{
    struct Codecraft *ads = &myCodecraft;
    char buffer[MAXPATHLEN];
	char valbuffer[20];
	BPTR file;
	BPTR olddir;
	struct CodecraftDoc *adsDoc;
	struct CodecraftDoc *currentAdsDoc = NULL;

	if (!ads->projectDirLock)
		return;

	olddir = CurrentDir(ads->projectDirLock);
	SNPrintf(buffer, sizeof(buffer), "%s.session",ads->project.name);
	file = Open(buffer, MODE_OLDFILE);
	CurrentDir(olddir);

    if (!file)
        return;

    FGets(file, buffer, MAXPATHLEN);

    if (Stricmp(buffer, "Codecraft session file\n")== 0)
    {
		while (TRUE)
        {
			Strncpy(buffer, "OPEN FILENAME \"", MAXPATHLEN);
			if (!FGets(file, buffer + 15, MAXPATHLEN - 15))
				break;
				
		    if (Stricmp(buffer+15, "BeginExpandedNodes\n")== 0)
		    {
			    expandNodes(ads, file, buffer);
			    break;
		    }
			Strncpy(buffer + strlen(buffer) - 1, "\"\n", 3);
			Strncpy(valbuffer, "GOTOLINE ", MAXPATHLEN);
			FGets(file, valbuffer + 9, 20-9);

			if (valbuffer[9] == '-')
			{
				adsDoc = (struct CodecraftDoc *)AllocMem(sizeof(struct CodecraftDoc), MEMF_CLEAR);

				adsDoc->ads = ads;

				Strncpy(adsDoc->fullPath, buffer + 15, MAXPATHLEN);
				adsDoc->fullPath[strlen(adsDoc->fullPath)-2] = 0;
				NewList((struct List *)&adsDoc->breakPointList);

				AddTail((struct List *)&ads->breakpointDocList, (struct Node *)adsDoc);

				FGets(file, valbuffer + 11, 20-11); // read and ignore the column position
			}
			else
			{
				ads->tei->executeRexxCommand(NULL, buffer);
				ads->tei->executeRexxCommand(NULL, valbuffer);

				Strncpy(valbuffer, "GOTOCOLUMN ", 20);
				FGets(file, valbuffer + 11, 20-11);
				ads->tei->executeRexxCommand(NULL, valbuffer);
				adsDoc = ads->currentAdsDocument;
			}

			if (!adsDoc)
				break;

            FGets(file, buffer, MAXPATHLEN);
            while (Stricmp(buffer, "EndOfBreakPoints\n") != 0)
            {
				BreakPointsToggleAtBlockNum(adsDoc, atoi(buffer));
                FGets(file, buffer, MAXPATHLEN);
            }

			do
			{
				FGets(file, buffer, MAXPATHLEN);
				if (Stricmp(buffer, "IsCurrentDocument\n") == 0)
					currentAdsDoc = adsDoc;
			}
			while (Stricmp(buffer, "EndOfPersistedFile\n") != 0);
        }
    }
    Close(file);

	if (currentAdsDoc)
	{
		SNPrintf(buffer, MAXPATHLEN, "OPEN FILENAME \"%s\"", currentAdsDoc->fullPath);
		ads->tei->executeRexxCommand(NULL, buffer);
	}
	RefreshWindowFrame(ads->window);
}
