![](images/logo.ilbm.png)

Welcome to Codecraft, the powerful Integrated Development Environment (IDE) for developing software
natively on the Amiga. Codecraft makes it easy for you as a developer to write code, then build,
execute and debug your resulting program. And everything is at your fingertips in one unified
user interface.

  - Shows your project tree as a hierarchical list of source files
  - Integrates the AmigaOS 3.2.2 TextEdit for easy modern text editing
  - Builds the project tree invoking your build system
  - Runs your resulting program
  - Does source level debugging with breakpoints and inspection of variables

### About this User's Guide

This User's guide has the following chapters:

  [Getting Started](#START)

  [Creating a ProjectTree](#CREATE)

  [Building and Running your Program](#BUILDANDRUN)

  [Using the Debugger](#DEBUGGING)

  [The Debugger Variable Browser](#VARBROWSER)

  [The Debugger Call Stack](#CALLSTACK)

  [Codecraft Settings](#SETTINGS)

  [About Project Templates](#PROJECTTEMPLATES)

  [Appendix](#Appendix)

# Getting Started {#START}

In this chapter you will learn how to install and start Codecraft
for the first time. You will also become familiar with the user
interface and open an example ProjectTree.

## Installing Codecraft

  1. Boot your Amiga from your hard disk and wait for Workbench to load.

  2. Insert the Codecraft disk.

  3. Double-click on the disk icon.

  4. Next double-click on the installer icon to start the installer.

  5. Follow the on screen instructions until Codecraft is installed.


Besides installing Codecraft you will need a working compiler and build system of your choice.

## Starting Codecraft

With the installation done you will find your Codecraft at the location you have chosen. Here you will find the program icon that you need to double click to start Codecraft.

![](images/Codecraft.info.png)

The icon on the right shows the optional Codecraft glow icon - available on the Codecraft _Install Disk_.

## Getting Familiar with the Codecraft Window

Codecraft opens up in a window. Codecraft is actually the same as TextEdit from AmigaOS 3.2 except Codecraft also adds several other GUI elements to the window. Once you have loaded a ProjectTree it will look like this:

![](images/ch1_pic1.png)

The window is divided into 4 main parts:
  
  1. The __ProjectTree Browser__ shows a list of files in your ProjectTree drawer. It also shows sub drawers in a hierarchical tree. It is more like a file browser, and is not integrated to your build system.

  1. The __Sourcefile Editor__ is where you edit your sourcefiles. If you have used TextEdit (part of AmigaOS 3.2) then it should feel quite familiar.

  1. The __Auxillary Browsers__ provides a lot of extra info. The Build log browser is useful when you are building. The Breakpoints, Variable and Call stack browsers are useful when debugging. These browsers will be covered in more detail in their respective chapters.

  1. The __Menus__ should also be halfway familiar if you have used TextEdit before. The Project, Edit and Filetype menus all relate to the individual files and not the ProjectTree. The ProjectTree, Build and Debug menus are specific to Codecraft.

## Opening the Example ProjectTree

Let us open an example ProjectTree. In the ProjectTree menu select the menu item named "Open". It will bring up a standard file requester where you should navigate to the place you have installed Codecraft There should be a drawer called "Example". Go into that drawer and then open the file called "example.ProjectTree".

As a result the ProjectTree browser will become populated and the files you had open last time will be reopened. Breakpoints you had set will be remembered. The example ProjectTree, as installed, will open as if you were in the middle of working with and will look like the above screenshot.

# Creating a ProjectTree {#CREATE}

In this chapter you will learn how to create a new ProjectTree. Because Codecraft offers a lot of flexibility you need to supply a number of properties before the ProjectTree is useful.	But it is easily done in a few minutes.

However, remember that Codecraft is not a build system nor a compiler. Please ensure you have installed those before proceeding.

### What is a ProjectTree?

Codecraft uses so called ProjectTrees to store and organize the structure and the settings of a project. The creation of a ProjectTree is an essential step when setting up a new development project. Later on, when working, you just open the already existing ProjectTrees.

A ProjectTree is simply stored as a file (with .projecttree extension), and therefore it can be managed by your source control system (e.g. git, subversion, cvs), like any other file of your development project.

## Creating a ProjectTree 

When creating a ProjectTree it can be either a completely new project or one based on existing source code already in a directory somewhere.

The remainder of this chapter will setup a C language project, but you can set it up for any language you like. You can even use if for non-coding projects like documentation writing where the build system converts say plain text to AmigaGuide or something similar.

To create a new ProjectTree, choose _New_ from the _ProjectTree_ menu.

![](images/ch2_pic1.PNG)

This will open the _New ProjectTree_ requester.

![](images/ch2_pic2.PNG)

With this requester, you setup how to create the ProjectTree. That is:

  - __Drawer__ The drawer of the ProjectTree. This is either your existing project drawer or a new drawer if you are creating a new ProjectTree from scratch or from template. If the drawer doesn't exist it will be created. 
  - __Name__ The name of the ProjectTree. This is the name of the project file in case you want more than one Projecttree inside the same drawer. But often you will simply name it the same as the drawer.
  - __Radio button__ ___From existing code___  The ProjectTree will use the existing files already in the drawer.
  - __Radio button__ ___From Scratch___ The ProjectTree is almost completely empty.
  - __Radio button__ ___From Template___ The ProjectTree is populated with the template that you can now select in the list below.

## ProjectTree Parameters Requester

To complete the setup of a new ProjectTree, after the _New ProjectTree_ requester was confirmed Codecraft presents automatically opens the _ProjectTree Parameters_ requester.

The general parameters are described here, but the build and debugging parameters are described in their respective chapters.

![](images/ch2_pic3.PNG)

  - __Project Drawer__ The drawer of the project, like given in the _New ProjectTree_ requester. This is not a property, but simply information of where the ProjectTree is located.

  - __Contained Files__ A list of AmigaDos wildcards (e.g. #?), that Codecraft will use to show files in the ProjectTree browser. You can think of it as filtered view of your drawer structure. Drawers are only shown if there are files inside that are visible according to the wildcards.

## Saving the ProjectTree

The ProjectTree is automatically saved whenever you start a build, switch to another ProjectTree or quit Codecraft.

So, in general, you don't have to save, but there is a _Save All_ menu item in the _ProjectTree_ menu that will explicitly save it and all the files.

## Opening a ProjectTree

To open an existing ProjectTree you can either choose _Open..._ in the _Project Tree_ menu, or use the _Recent_ menu to reopen a previously opened ProjectTree.

# Building and Running your Program {#BUILDANDRUN}

In this chapter you will learn how to build your program. If your build system produces warnings and errors you will learn how to go through each of those. Finally you will learn how to run your program.

It is actually pretty simple.
 
## The Related ProjectTree Parameters

In the ProjectTree parameters requester you will find several settings relevant for building
and running your program.

![](images/ch3_pic1.PNG)

  - __Build Drawer__ Before building the current directory will be changed to this location. You can specify an absolute or relative path, or simply leave it blank to stay in the project drawer

  - __Build Command__ The command used to build your project. It is typically _make_ or _smake_ or something similar.

  - __Clean Command__ The command used to clean your project tree from build files. It is typically _make clean_ or similar.

  - __Up-to-date Command__ If supported by your build system, you can enter a command or script that is executed by Codecraft before running your application. The command should check whether the project is up-to-date or needs a build. A return value of 0 (zero) means that the project is current, any other return value indicates that a build is necessary. In that case, Codecraft will show a corresponding requestor.

## The Build Menu

The build menu is where you actually start the build.

![](images/ch3_pic2.PNG)

  - __Build__ This starts you build command as specified in ProjectTree parameters. First the current drawer is changed to the project drawer, and from there the current drawer is changed to your build drawer. Build drawer can be empty, relative or absolute.

  - __Clean__ This starts the clean command. The current drawer is set up like the _Build_ menu item above.

  - __Rebuild__ This menu item simply executes _Build_ and _Clean_ menu items in succession.

  - __Previous Message__ Jumps to the previous message in the build log.

  - __Next Message__ Jumps to the next message in the build log.

## Viewing the Build Log and Jumping to Errors

When you have started you build the output from the build system will start to appear in the build log. The build log is automatically shown, so you should soon see the messages scroll by.

__Limitation:__ The detection of error and warning messages, is highly dependent on the compiler. For now we can only jump to messages from SAS/C and VBCC, but if you have examples from other compilers get in touch, and we will try and add support.

If you double click on a line with warning or error, Codecraft takes you automatically to the corresponding file and line of code.

The menu items _Previous Message_ and _Next Message_ along with their shortcuts (Shift) F8 will also bring you to the file and line in question.

## Running your Application

There are either two options in the _Debug_ Menu to run your application:

__Run In Debugger__  
Starts your application for debugging.

__Run Without Debugger__  
Starts your application as if it would be started from Workbench or CLI. 

# Using the Debugger {#DEBUGGING}

Codecraft comes with a built-in source level debugger. A debugger is one of the most essential tools when making your program bug free.

The debugger is capable of stopping execution at breakpoints.

### Stopping a Program is Impossible on the Amiga

It is important that the user manually stops a running program in the debugger. The Amiga is limited in what we can do here, so for now at least it is better to stop the program gracefully.

The reason is that the Amiga doesn't have resource tracking, so while we can stop the program from actively running we cannot safely free memory allocated by it or close its windows etc, etc.

### Limited debugging with other compilers than SAS/C

For now the debugger can only show values for programs compiled with SAS/C 6.58 and with debug=symbol.

It is intended to expand this functionality to other compilers and languages in the future.

Any executable that has debug LINE hunks can be debugged using breakpoints and stepping. Many compilers besides SAS/C produces this already. But you will not be able to see variables and their values.

## The Debug Menu

The debug menu is where you run your app and control __Codecrafts__ integrated debugger.

![](images/ch4_pic2.PNG)

  - __Run In Debugger__ Starts your application for debugging by loading the debug information and prepares the breakpoints. Execution will break, as soon as a breakpoint is hit.

  - __Run Without Debugger__ Simply starts your application as if it would be started from Workbench or CLI. The debug information of you application is not loaded and breakpoints are ignored.

  - __Break__ Breaks the current execution immediately.  

  - __Resume__ Continues the execution of your application when a breakpoint was hit or the execution was stopped using _Break_.

  - __Resume Without Debugger__ Continues the execution of your application by temporarily disabling all breakpoints. This allows to gracefully run the application to end, without further interruption by breakpoints. The breakpoints are re-enabled on the next debugger run. 
  Because of the shared memory architecture of the Amiga, running debugging sessions cannot terminated immediately without endangering the stability of the system. Therefore, it is necessary to end running debugging sessions by gracefully exiting the debugged application. And for doing so, _Resume Without Debugger_ is very convenient. 

  - __Step Into__ Executes the next program line. Steps into functions, if the program line to execute is a function call.

  - __Step Over__ Executes the next program line, but steps over function calls by executing the whole function.

  - __Step Return__ Continues execution until the current function is exited, then breaks the execution.

  - __Toggle Breakpoint__ Sets or removes a breakpoint on the current position of the cursor, or on the nearest code line possible.

  - __Delete All Breakpoints__ All breakpoints are removed immediately.

  - __Disable All Breakpoints__ All breakpoint are disabled - i.e. they stay in place but the debugger does not stop when a breakpoint is hit.

## The Related ProjectTree Parameters

![](images/ch4_pic1.PNG)

  - __Command__ The name of your executable. You can specify either a path relative to you _build_ drawer, or an absolute path.

  - __Command Arguments__ The arguments to supply to your program.

  - __Working Drawer__ The drawer where the program is running. You maybe have some test files here etc.

  - __Start as if From__ Radio buttons to choose whether you application is started as CLI or Workbench application. When from _Workbench_ is selected, your application gets send the usual WBStartup message.

In general, Codecraft runs you application with a Stack size of 100000.
   
## The Breakpoints Browser

The Break Points Browser is located in the tab __Breakpoints__, below the code editor in the Codecraft window.

![](images/ch5_pic1.PNG)

The Break Point Browser lists all breakpoints in the current ProjectTree by displaying the file and line the breakpoint is set.
By double clicking an entry, Codecraft takes you to the file and line of the breakpoint. Further, by checking/unchecking the checkbox, you can enable/disable the corresponding breakpoint.

# The Debugger Variables Browser {#VARBROWSER}

When you run your application in debug mode (see Using the Debugger) and a breakpoint is hit, the application execution breaks. In this state you can watch different information of your app.

One of this information is the current values of the variables of your application. The variables are displayed by switching to the ___Variables___ tab in bottom tab bar in the Codecraft user interface:

![](images/ch6_pic1.PNG)

The variables and their values are displayed in three columns:

__First Column__ The first column shows the name of the variable and some additional information. If the variable is a complex element like an _Array_ or _Struct_ for example, the variable can be expanded and nested elements are displayed. If a variable is stored in a register, the name of the register is displayed.

__Second Column__ The second column shows the value of the variable. The display-format of the value is chosen automatically by Codecraft, depending on the variable type.

__Third Column__ The third column shows the datatype of the variable. Datatype names are shown as used in the code. This means that typedefs are displayed as is and are not substituted by their basic type names.

# The Debugger Call Stack {#CALLSTACK}

When your application is stopped by hitting a breakpoint, you can inspect the call hierarchy of your application using the ___Call stack___ tab, also located in the bottom tab bar.

![](images/ch7_pic1.PNG)

The call stack shows the source file, name of the function and line number your application has called to reach the current breakpoint. The blue arrow on the first entry of the call stack indicates the position of the current program execution.

When you double click on the other entries in the call stack Codecraft navigates you to the corresponding line of code. At the same time the entry you double clicked will get a blue curved arrow, and  the variable browser is updated to reflect the variables at that place in the code.

# Codecraft Settings {#SETTINGS}

In addition to the settings that TextEdit provides, Codecaft provides some more specific settings.

![](images/ch8_pic1.PNG)

  - __Switch Tab on Break__ provides the option to make Codecraft automatically switch to a specific tab of the bottom tab bar during debugging, when a breakpoint is hit. You can choose either the _Variables_ tab, the _Call stack_ tab or not to switch to another tab.

# About Project Templates {#PROJECTTEMPLATES}

Project Templates are a convenient and easy way to create a base for a new development project in Codecraft. Project Templates are presented in the _New ProjectTree_ requester.

A Project Template usually consists of a collection of files and resources that are prefilled with code and data, to create a specific development project from it. Like a CLI application, an Amiga library, etc.

When creating a new Project Tree from a template, Codecraft copies the files and data of the project template to the newly created Project Tree directory, and adjusts the content of the copied template files according to your new Project Tree specifics.

## Templates shipped with Codecraft

Currently all ProjectTree Templates shipped with Codecraft are templates SAS/C development projects:

__Simple Workbench Application__  
The _Simple Workbench Application_ template creates a project with necessary startup code to run as a Workbench application. That is responding to the Workbench startup message and parsing the application Tooltypes.

__Simple Commandline Application__  
The _Simple Commandline Application_ template creates a command line application, running in the Shell. The template code also reads some command line parameters, as a starting point for your own implementation.

__BOOPSI Window Application__  
The _BOOBSI Window Application_ template creates a Workbench application that just opens an empty window. It is a good starting point for any gui application. 

## Creating custom templates

Codecraft comes already with a set of predefined project templates. However, it is easily possible to add further, individual templates for your own development projects. For this, a separate folder for user templates is created when Codecraft was installed. That folder is named  _UserTemplates_ and it is located in the Codecraft program folder.

To create your own project templates, you have to create an folder within the _UserTemplates_ folder for each of your project templates. Create all files of your project template in the respective project template directory, like:

  - source files
  - smake file
  - ProjectTree file for your template
  - all sorts of other resources for your template (images, sources, whatsoever)

## Important conventions for project template files

In order for Codecraft to be able to customize the files of a project template when creating a plain ProjectTree from it, the following conventions must be used:

### Renaming of files 
 
Any file which name is or contains "TEMPLATE" will be renamed during project creation by replacing "TEMPLATE" with the name of the Project Tree.

__Example:__

Let's assume you create a template that should contain a #?.c file with some code and a #?.info icon file. When creating a project from the template, you want the c file and the icon get renamed with the project name.

_Names of the template files:_  
TEMPLATE.info  
TEMPLATE.c  
  
_Names after project creation (e.g. MyAmigaApp.projecttree):_  
MyAmigaApp.info  
MyAmigaApp.c  
  
### Replacing of text tokens

It is possible to insert the name of your project  (= ProjectTree name) in the files when a project is created from a template. E.g. for the name of the binary to be created by the makefile, of just the application name shown in the window title.
Codecraft searches for the token _%TEMPLATE%_ in all files of your template (except binary files) and will replace _%TEMPLATE%_ by your project name.

__Example:__

Let's assume the name of your project is MyAmigaApp and you want the name to appear in the window title of your workbench application.

_Code in the template that creates a window:_  
windowObject = NewObject(WINDOW_GetClass(), NULL,  
   WINDOW_Position, WPOS_CENTERSCREEN,  
   WA_Activate, TRUE,  
   WA_Title, __"%TEMPLATE%"__,  
   WA_DragBar, TRUE,  
   WA_CloseGadget, TRUE,  
   etc...  
  
_Code after project creation:_  
windowObject = NewObject(WINDOW_GetClass(), NULL,  
   WINDOW_Position, WPOS_CENTERSCREEN,  
   WA_Activate, TRUE,  
   WA_Title, __"MyAmigaApp"__,  
   WA_DragBar, TRUE,  
   WA_CloseGadget, TRUE,  
   etc...  
  
This feature is also very handy to adjust smakefile files included in templates.
For further reference and more examples, you can inspect the project templates provided with Codecraft, they are located in the _Templates_ folder inside the Codecraft application folder.

One further note: You can organize your templates by putting them into subfolders within the _UserTemplates_ folder. This will make Codecraft to show the templates in the _New ProjectTree_ Dialog sorted into groups, named by the folder name.

# Appendix {#Appendix}

## About the Codecraft Project

For news and to download the latest version of Codecraft, visit the official Codecraft homepage at: http://boemann.dk/codecraft/.

Codecraft development is hosted on GitLab, see: https://gitlab.com/boemann/codecraft.

Todo: How to report bugs?
Todo: How to submit suggestions?
-> Maybe a form on the Website?

## Copyright  

Codecraft is Copyright 2022-2023 by Camilla Boemann and Ralf Hasemann.
 
Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 2 as published by the Free Software Foundation. 

But note, that it is made like a plugin into TextEdit of AmigaOS 3.2 which has its own license.

Codecraft is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

For more information about the GNU General Public License, see https://www.gnu.org/licenses/.

## Requirements  

__Hardware__  
Technically, Codecraft runs an Amiga with a plain 68000 CPU, some Fast RAM (e.g. 4MB) and a hard drive to hold the system and the development environment. However, such a system would be really slow and compiling stuff would take a long time.  

Therefore, we recommend the following system equipment to run development using Codecraft:

  - Amiga with 68030 CPU running at 25MHz
  - 8 MB Fast RAM
  - Flicker Fixer to run PAL Highres Interlaced
  - Hard drive or SD Card

These are the minimum requirements to work meaningfully with Codecraft. A faster system makes it even better! 

__Software__  
At least AmigaOS 3.2.2 is currently required to run Codecraft 1.0 and greater. Since Codecraft relies heavily on TextEdit, which is part of the AmigaOS distribution, AmigaOS 3.2.2 is a mandatory requirement.

## SRC6 Hunk Documentation

As part of the Codecraft project, the Amiga SRC6 hunk format, as generated by SAS/C, was reverse engineered. This was necessary to develop the Codecraft Debugger. Because of the complexity of the SRC6 Hunk format, this is still a work in progress.  

You can find the current documentation of the SRC6 hunk format on the Codecaft GitLab page:

https://gitlab.com/boemann/codecraft/-/blob/master/SRC6%20description.txt