#define __CLIB_PRAGMA_LIBCALL
#include <proto/exec.h>
#include <proto/dos.h>


char *vers="\0$VER: %TEMPLATE% "__AMIGADATE__""; // AMIGADATE is a SAS/C speciality
char *stacksize = "$STACK:8192"; // only works when started from CLI

/* define your command template and options */
#define TEMPLATE       "FANCY/K,FILE/M"
#define OPT_FANCY 0
#define OPT_FILENAME 1
#define OPT_COUNT 2


int main(int argc, char **argv)
{
	struct RDArgs *rdargs = NULL;
	LONG opts[OPT_COUNT] = { NULL };

	Printf("%TEMPLATE%\n");

	if (rdargs = ReadArgs(TEMPLATE, opts, NULL))
	{
		if (opts[OPT_FANCY])
				Printf("you selected fancy: %s\n", (STRPTR) opts[OPT_FANCY]);

		if (opts[OPT_FILENAME])
		{
				STRPTR *filenames = (STRPTR *) opts[OPT_FILENAME];
				while (*filenames)
				Printf("on file: %s\n", *(filenames++));
		}
	}
	else
	{
		PrintFault(IoErr(), NULL);
		return RETURN_ERROR;
	}
	return RETURN_OK;
}

