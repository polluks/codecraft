#include <workbench/startup.h>
#include <workbench/icon.h>

#define WBREPLYPORTNAME "CodecraftWbStartReplyPort"

struct WbRun
{
	struct WBStartup WBStartup; /* must be first member! */
	ULONG Size;
	struct WBArg WBArg[1];
};
