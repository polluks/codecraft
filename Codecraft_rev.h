#define VERSION		1
#define REVISION	14
#define DATE		"22.9.2023"
#define VERS		"Codecraft 1.14"
#define VSTRING		"Codecraft 1.14 (22.9.2023)\r\n"
#define VERSTAG		"\0$VER: Codecraft 1.14 (22.9.2023)"
