/* This file is part of Codecraft, an IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecrafr is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

#include <dos/dostags.h>
#include <dos/doshunks.h>

#include <tools/textedit/extension.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>
#include <proto/intuition.h>
#include <proto/alib.h>

#include "codecraft.h"
#include "debugger.h"

enum DecendMode {
	ONLYPARSE = 0,		  // parse so location is advanced
						  // but never actually add anything
	DECL,				  // Add the tag, but not members
						  // as members are added by expanding
						  // However it is important to decend
						  // and parse the members to get the correct
						  // following location.
						  // This is also important for substructs
	POSTPONEDDECL,		  // For calling ParseID when we know the ID
						  // is a typedef and not a decl.
	DECLBUTNOTTAGITSELF   // Used when user expands but in ParseTag
						  // it is transformed into DECL, as soon
						  // as we have skipped adding the tag
						  // itself
};

struct SourceDumpInfo
{
	struct Node node;
	struct VariableBrowserInfo vbi;
	TEXT filename[256];
	ULONG initializedNearOffset;
	ULONG initializedFarOffset;
	ULONG uninitializedNearOffset;
	ULONG uninitializedFarOffset;
	ULONG chipOffset;
	ULONG initializedNearHunk;
	ULONG initializedFarHunk;
	ULONG uninitializedNearHunk;
	ULONG uninitializedFarHunk;
	ULONG chipHunk;
	UBYTE *dataMem[8];
	
	UBYTE *buffer;
	STRPTR stringTable;
	UBYTE *locationZero;
	ULONG GIHLocation;
	ULONG NumFuncs;
	ULONG ExtLocation;
	ULONG StaticLocation;
	ULONG FMAPLocation;
	ULONG IsAReg;
	ULONG handledFirstTypedef;
};

ULONG ParseMember(struct SourceDumpInfo *sdl, ULONG location,
	ULONG level, enum DecendMode decl, UBYTE *memPtr);
ULONG ParseID(struct SourceDumpInfo *sdl, ULONG location,
	ULONG level,
	enum DecendMode decl, UBYTE *memPtr);

extern struct DebuggerRegisters *brokenRegs;

void clearEntryInfo(struct SourceDumpInfo *sdl)
{
	struct VariableBrowserInfo *vbi = &sdl->vbi;
	
	*vbi->nameString = *vbi->typeString = *vbi->valueString
		= *vbi->ptrString = *vbi->funcPtrString
		= *vbi->encodedTypeString = 0;
	sdl->handledFirstTypedef = FALSE;
	sdl->IsAReg = FALSE;
}

void RegisterData(struct Node *node, ULONG hunkNum, UBYTE *memPtr)
{
	struct SourceDumpInfo *sdl = (struct SourceDumpInfo *)node;

	if (!sdl)
		return;
		
	while (sdl->node.ln_Succ)
	{
		sdl->dataMem[hunkNum] = memPtr;
		sdl = (struct SourceDumpInfo *)sdl->node.ln_Succ;
	}
}

void RegisterBSS(struct Node *node, ULONG hunkNum, UBYTE *memPtr)
{
	struct SourceDumpInfo *sdl = (struct SourceDumpInfo *)node;

	if (!sdl)
		return;
		
	while (sdl->node.ln_Succ)
	{
		sdl->dataMem[hunkNum] = memPtr;
		sdl = (struct SourceDumpInfo *)sdl->node.ln_Succ;
	}
}

void ParseOffsets(struct SourceDumpInfo *sdl, UBYTE *buf)
{
	ULONG *ptr = (ULONG *)buf;
	
	sdl->initializedNearHunk = (*ptr) >>24;
	sdl->initializedNearOffset = (*ptr++) & 0xFFFFFF;
	sdl->initializedFarHunk = (*ptr) >>24;
	sdl->initializedFarOffset = (*ptr++) & 0xFFFFFF;
	sdl->uninitializedNearHunk = (*ptr) >>24;
	sdl->uninitializedNearOffset = (*ptr++) & 0xFFFFFF;
	sdl->uninitializedFarHunk = (*ptr) >>24;
	sdl->uninitializedFarOffset = (*ptr++) & 0xFFFFFF;
	sdl->chipHunk = (*ptr) >>24;
	sdl->chipOffset = (*ptr) & 0xFFFFFF;
}

void ParseHeader(struct SourceDumpInfo *sdl)
{
	UBYTE *buf = sdl->buffer;
	ULONG stringTableSize;
	ULONG stringsLocation;
	ULONG afterStringsLocation;
	ULONG aligner[4] = {0, 3, 2, 1};

	sdl->locationZero = buf + 12 + *((ULONG*)(buf+8));

	sdl->GIHLocation = *((ULONG*)sdl->locationZero);
	
	Strncpy(sdl->filename, buf+12, sizeof(sdl->filename));
	
	stringsLocation = *((ULONG*)(sdl->locationZero+4));
	sdl->stringTable = sdl->locationZero + stringsLocation;
	
	stringTableSize = *((ULONG*)(sdl->locationZero+8));

	afterStringsLocation = stringsLocation + stringTableSize;
	
	// it seems tablesize is always long aligned
	ParseOffsets(sdl, sdl->stringTable + stringTableSize
	+ aligner[afterStringsLocation&3]);
}

void ParseGIH(struct SourceDumpInfo *sdl)
{
	UBYTE *buf = sdl->locationZero;
	ULONG location = sdl->GIHLocation;
	ULONG flags = *(buf + location);

	location++;
	
	sdl->NumFuncs = 0;
	sdl->ExtLocation = 0;
	sdl->StaticLocation = 0;
	
//	kprintf("%4ld [%lx] GIH -\n",sdl->GIHLocation, *buf);
	if (flags & 0x01)
	{
		sdl->NumFuncs = *((UWORD*)(buf+location));
		location += 2;
	}
	if (flags & 0x02)
	{
		location += 4;
	}
	if (flags & 0x04)
	{
		location += 4;
	}
	if (flags & 0x08)
	{
		sdl->StaticLocation = *((ULONG*)(buf+location));
		location += 4;
	}
	if (flags & 0x10)
	{
		sdl->ExtLocation = *((ULONG*)(buf+location));
		location += 4;
	}
	
	sdl->FMAPLocation = location;
}

ULONG sizeOfBasicType(ULONG typeid)
{
	ULONG size[] =
	{
		sizeof(signed char),
		sizeof(signed short),
		sizeof(signed long),
		4,//sizeof(signed long long),
		sizeof(float),
		sizeof(double),
		sizeof(long double),
		sizeof(void),
		sizeof(unsigned char),
		sizeof(unsigned short),
		sizeof(unsigned long),
		4//sizeof(unsigned long long)
	};
	return size[(typeid & 0xF)];
}

void printBasicType(struct SourceDumpInfo *sdl, ULONG typeid)
{
	STRPTR types[] =
	{
		"signed char ",
		"signed short ",
		"signed long ",
		"signed long long ",
		"float ",
		"double ",
		"long double ",
		"void ",
		"unsigned char ",
		"unsigned short ",
		"unsigned long ",
		"unsigned long long "
	};
	Strncat(sdl->vbi.typeString, types[(typeid & 0xF)], MAXTYPELEN);
}

// Returns TRUE if  basic value or a pointer to basic vlaue
// Will also return FALSE if function pointer
ULONG isValueOrPtrTo(STRPTR encodedTypeString)
{
	if (encodedTypeString[9] == 0)
		return TRUE;
		
	if (encodedTypeString[9] == '*' && encodedTypeString[10] == 0)
	    return TRUE;
	    
	return FALSE;	
}

ULONG isStrPtr(STRPTR encodedTypeString)
{
	if (encodedTypeString[9] == 0)
		return FALSE;
		
	if (encodedTypeString[9] == '*' && encodedTypeString[10] == 0)
		return TRUE;

	if (encodedTypeString[9] == '[' && encodedTypeString[11] == 0)
		return TRUE;
		
	return FALSE;
}

void writeStrFmtStr(STRPTR format, STRPTR s)
{
	int cnt = 0;

	while (*(s++) && cnt < 31)
		cnt++;
		
	if (cnt > 30)
		strcpy(format, "%s<addr> 0x%08lx (\"%.30s\"...)");
	else
		strcpy(format, "%s<addr> 0x%08lx (\"%.30s\")");
}

void printBasicValue(struct SourceDumpInfo *sdl, ULONG typeid
	, UBYTE *memPtr, UBYTE bitFieldPos, UBYTE bitFieldWidth)
{
	ULONG value = 0;
	ULONG bitWidth;
	DOUBLE dvalue;
	TEXT tmpBuf[40];
	STRPTR format = tmpBuf;
	STRPTR ptrString = sdl->vbi.ptrString;
	LONG illegal = FALSE;

	if ((typeid & 0xF) == 7 || ! isValueOrPtrTo(sdl->vbi.encodedTypeString))
	{
		if (sdl->vbi.encodedTypeString[10] == 'F' )
			SNPrintf(sdl->vbi.valueString, sizeof(sdl->vbi.valueString),"<func> 0x%08lx", memPtr);
		else
			SNPrintf(sdl->vbi.valueString, sizeof(sdl->vbi.valueString),"<addr> 0x%08lx", memPtr);
		return;
	}
		 
	if (*ptrString)
	{
		SNPrintf(sdl->vbi.valueString, sizeof(sdl->vbi.valueString),"<addr> 0x%08lx (", memPtr);
	}
	
	if (TypeOfMem(memPtr))
	{
		switch (typeid & 0xF)
		{
			case 0:
				if (isStrPtr(sdl->vbi.encodedTypeString))
				{
					value = *(ULONG *)memPtr;					
					writeStrFmtStr(format, (STRPTR)value);
				}
				else
				{
					if (sdl->IsAReg)
						value = *(BYTE *)(memPtr+3);
					else
						value = *(BYTE *)memPtr;
							
					if (isprint(value))
						format = "%s%ld ('%lc')";
					else
						format = "%s%ld (0x%02lx)";
				}
				bitWidth = 8;
				break;
				
			case 1:
				if (sdl->IsAReg)
					memPtr += 2;
				value = *(WORD *)memPtr;
				format = "%s%ld (0x%04lx)";
				bitWidth = 16;
				break;
				
			case 2:
				value = *(LONG *)memPtr;
				format = "%s%ld (0x%08lx)";
				bitWidth = 32;
	
				break;
				
			case 3:
				value = *(LONG *)memPtr;
				format = "%s%ld (0x%08lx)";
				bitWidth = 32;
				break;
				
	/*		case 4:
				value = *(FLOAT *)memPtr;
				format = "%s%lf";
				break;
				
			case 5:
				dvalue = *(DOUBLE *)memPtr;
				format = "%s%lf";
				break;
				
			case 6:
				dvalue = *(DOUBLE *)memPtr;
				format = "%s%lf";
				break;
	*/			
			case 7:
				value = 0;
				format = "%s";
				break;
				
			case 8:
				if (isStrPtr(sdl->vbi.encodedTypeString))
				{
					value = *(ULONG *)memPtr;
					writeStrFmtStr(format, (STRPTR)value);
				}
				else
				{
					if (sdl->IsAReg)
						value = *(UBYTE *)(memPtr+3);
					else
						value = *(UBYTE *)memPtr;
							
					if (isprint(value))
						format = "%s%lu ('%lc')";
					else
						format = "%s%lu (0x%02lx)";
				}
				bitWidth = 8;
				break;
				
			case 9:
				if (sdl->IsAReg)
					memPtr += 2;
				value = *(UWORD *)memPtr;
				format = "%s%lu (0x%04lx)";
				bitWidth = 16;
				break;
				
			case 10:
				value = *(ULONG *)memPtr;
				format = "%s%lu (0x%08lx)";
				bitWidth = 32;
				break;
				
			case 11:
				value = *(ULONG *)memPtr;
				format = "%s%lu (0x%08lx)";
				bitWidth = 32;
				break;
				
		}
		if (bitFieldWidth)
		{
			if (typeid & 0x08)
			{
				value >>= (bitWidth - bitFieldPos - bitFieldWidth);
				value &= (1<<bitFieldWidth)-1;
			}
			else
			{
				//signed
				LONG svalue = (LONG)value;
				svalue <<= (32 - bitWidth + bitFieldPos);
				svalue >>= (32 - bitFieldWidth);
				value = (ULONG)svalue;
			}
		}	
	}
	else
	{
		if (memPtr == (void *)~0)
			format = "optimized away";
		else
		{
			format = "";
			illegal = TRUE;
		}
	}
		
	SNPrintf(sdl->vbi.valueString, sizeof(sdl->vbi.valueString),format,
			 sdl->vbi.valueString, value, value);

	if (illegal)
		SNPrintf(sdl->vbi.valueString, sizeof(sdl->vbi.valueString),"<illegal addr> 0x%08lx", memPtr);
	else
		if (*ptrString )
			Strncat(sdl->vbi.valueString, ")", MAXTYPELEN);
}

void ExpandArray(struct SourceDumpInfo *sdl, STRPTR encodedType,
		ULONG level, UBYTE *memPtr,
		ULONG elementSize, ULONG skipPtrSteps)
{
	ULONG arrayLength = 0;
	ULONG i;
	STRPTR endp;
	ULONG pos = 9;
	ULONG location = (ULONG)strtoul(encodedType+1, NULL, 16);

	// we have continuation
	if (encodedType[pos] == '*')
	{
		skipPtrSteps++;
//				memPtr = *(UBYTE **)memPtr;
		pos++;
	}
	
	if (encodedType[pos] == '[')
	{
		arrayLength = (ULONG)strtoul(encodedType+pos+1, &endp, 10);
		skipPtrSteps++;
	}
	else if (encodedType[pos] == '*')
	{
		arrayLength = 1;
		skipPtrSteps++;
	}
	
	// See if our elements are pointers
 	while (encodedType[++pos])
		if (encodedType[pos] == '*')
		{
			elementSize = 4;
			break;
		}
		
	for (i = 0; i < arrayLength; i++)
	{
		sdl->vbi.skipPtrSteps = skipPtrSteps;
		sdl->vbi.totalSkipPtrSteps = skipPtrSteps;

		SNPrintf(sdl->vbi.nameString, MAXTYPELEN, "[%ld]", i);
		if (*encodedType == 'M')
			ParseMember(sdl, location, level, DECL, memPtr);
		else
			ParseID(sdl, location, level, DECL, memPtr);
		memPtr += elementSize;
	}
}

ULONG PrintEnumValue(struct SourceDumpInfo *sdl, ULONG location, ULONG val)
{
	ULONG done = FALSE;
	ULONG found = FALSE;
		
	while (! done)
	{
		if (!found && *(ULONG *)(sdl->locationZero + location + 9) == val)
		{
			Strncpy(sdl->vbi.valueString, sdl->stringTable + *(ULONG *)(sdl->locationZero + location + 1), MAXTYPELEN);
			found = TRUE;
		}
		done = (sdl->locationZero[location] & 0x02) == 0;
		location += 13;
	}
				
	SNPrintf(sdl->vbi.valueString, sizeof(sdl->vbi.valueString),"%s (%ld, 0x%08lx)", sdl->vbi.valueString, val, val);

	return location;
}

ULONG ParseTag(struct SourceDumpInfo *sdl, ULONG location,
	ULONG level,
	enum DecendMode decl, UBYTE *memPtr)
{
	UBYTE *buf = sdl->locationZero + location;

//	kprintf("%4ld [%lx] TAG - ",location, *buf);

	if (*buf & 0x38)
	{
		ULONG done = FALSE;
		STRPTR name = sdl->stringTable + *(ULONG *)(buf + 1);
		ULONG expand = FALSE;
		ULONG isNotFunctionPtr = (sdl->vbi.encodedTypeString[9]
			 != '*' || sdl->vbi.encodedTypeString[10] != 'F');
		ULONG simpleTag = (sdl->vbi.encodedTypeString[9]
			 == 0 || sdl->vbi.encodedTypeString[10] == 0);
		ULONG isEnum = (*(ULONG *)(buf + 5) & 0x02);		

		if (*(ULONG *)(buf + 5) & 0x01)
			Strncat(sdl->vbi.typeString, "union ", MAXTYPELEN);
		else if (*(ULONG *)(buf + 5) & 0x02)
			Strncat(sdl->vbi.typeString, "enum ", MAXTYPELEN);
		else
			Strncat(sdl->vbi.typeString, "struct ", MAXTYPELEN);
		
		if (*name)
			Strncat(sdl->vbi.typeString, sdl->stringTable + *(ULONG *)(buf + 1), MAXTYPELEN);
		else
			Strncat(sdl->vbi.typeString, "<unnamed>", MAXTYPELEN);
		
		Strncat(sdl->vbi.typeString, " ", MAXTYPELEN);
		Strncat(sdl->vbi.typeString, sdl->vbi.ptrString, MAXTYPELEN);
		Strncat(sdl->vbi.typeString, sdl->vbi.funcPtrString, MAXTYPELEN);

		if (isValueOrPtrTo(sdl->vbi.encodedTypeString))
		{
			SNPrintf(sdl->vbi.encodedTypeString, MAXTYPELEN, "S%08lx", location);
		}

		sdl->vbi.elementSize *= *(ULONG *)(buf + 9);
		
		location += 13;

		{
			if (isNotFunctionPtr)
			{
				if (isEnum)
					location = PrintEnumValue(sdl, location, *(LONG *)memPtr);
				else
					SNPrintf(sdl->vbi.valueString, sizeof(sdl->vbi.valueString),"<addr> 0x%08lx", memPtr);
			}
			else
				SNPrintf(sdl->vbi.valueString, sizeof(sdl->vbi.valueString),"<func> 0x%08lx", memPtr);

			if (decl != ONLYPARSE)
			{
				if (decl != DECLBUTNOTTAGITSELF)
					expand = createOrUpdateEntry(level, &sdl->vbi,
						isNotFunctionPtr && !isEnum, memPtr);
				else
				{
					decl = DECL;
					expand = TRUE;
				}
			}

			if (simpleTag)
			{
				clearEntryInfo(sdl);

				if (!expand)
					decl = ONLYPARSE;
					
				if (!isEnum)
				{
					// since not an enum we will parse members
					while (! done)
					{
						done = (sdl->locationZero[location] & 0x10) == 0;
						location = ParseMember(sdl, location, level+1, decl , memPtr);
					}
				}
			}
			else if (expand)
			{
				TEXT encodedType[sizeof(sdl->vbi.encodedTypeString)];
				
				Strncpy(encodedType, sdl->vbi.encodedTypeString, sizeof(encodedType));
				clearEntryInfo(sdl);
				ExpandArray(sdl, encodedType, level+1, memPtr,
					 sdl->vbi.elementSize, sdl->vbi.skipPtrSteps);
			}
			clearEntryInfo(sdl);
		}
		return location;
	}
	else
	{
		return location + 5;
	}

}

ULONG ParseContinuation(struct SourceDumpInfo *sdl, ULONG *ptrDepth, ULONG location,
 ULONG *sizeMultiplier)
{
	UBYTE *buf = sdl->locationZero + location;
	ULONG flags = *buf;

//	kprintf("%4ld [%lx] CTX - ",location, *buf);
	
	location++;
	
	if (flags & 0x1) // function
	{
		if (*ptrDepth >= sdl->vbi.skipPtrSteps)
			;
		if (!sdl->handledFirstTypedef)
		{
			SNPrintf(sdl->vbi.funcPtrString, MAXTYPELEN, "(%s)()", sdl->vbi.ptrString);
			*sdl->vbi.ptrString = 0;
		}
		Strncat(sdl->vbi.encodedTypeString, "F", MAXTYPELEN);
	}
	
	if (flags & 0x4) // array
	{
		if (*ptrDepth >= sdl->vbi.skipPtrSteps)
		{
			ULONG len = *(ULONG *)(buf + 1);
			if (len == ~0)
			{			
				Strncat(sdl->vbi.encodedTypeString, "[]", MAXTYPELEN);
			}
			else
			{
				SNPrintf(sdl->vbi.encodedTypeString, MAXTYPELEN, "%s[%ld]", sdl->vbi.encodedTypeString, len);

				if (!sdl->handledFirstTypedef)
				{
					if (*sdl->vbi.ptrString == '*')
					{
						strins(sdl->vbi.ptrString, "(");
						SNPrintf(sdl->vbi.ptrString, MAXTYPELEN, "%s)[%ld]", sdl->vbi.ptrString, len);
					}
					else
						SNPrintf(sdl->vbi.ptrString, MAXTYPELEN, "%s[%ld]", sdl->vbi.ptrString,len);
				}
				
				if (*sizeMultiplier)
					*sizeMultiplier *= len;
				else
					*sizeMultiplier = 1;
			}
		}
		location += 4;
	}
	
	if (flags & 0x02) //pointer
	{
		if (*ptrDepth >= sdl->vbi.skipPtrSteps)
		{
			Strncat(sdl->vbi.encodedTypeString, "*", MAXTYPELEN);
		
			// Insert * at beginning of sdl->vbi.ptrString
			if (!sdl->handledFirstTypedef)
	 			strins(sdl->vbi.ptrString, "*");
		}			
		
	}
	
	(*ptrDepth)++;
	if (flags & 0x08) //there is more
		location = ParseContinuation(sdl, ptrDepth, location, sizeMultiplier);
		
	return location;
}


ULONG ParseMember(struct SourceDumpInfo *sdl, ULONG location, ULONG level,
	enum DecendMode decl, UBYTE *memPtr)
{
	UBYTE *buf = sdl->locationZero + location;
	ULONG flags = *buf;
	ULONG offset;
	ULONG ptrDepth = 0;

//	kprintf("%4ld [%lx] MEM - ",location, *buf);
		
	SNPrintf(sdl->vbi.encodedTypeString, MAXTYPELEN, "M%08lx", location);

	location += 13;
	
	if (flags & 0x21) // ref to another typdef or enum
		location += 4;// for the extra ref
	if (flags & 0x40) // bitfield
		location += 2;

	offset = *(ULONG *)(sdl->locationZero + (location - 4));

	if (! *sdl->vbi.nameString)
	{
		// we are populating the members as opposed to
		// expanding a member that is an array
		Strncat(sdl->vbi.nameString, sdl->stringTable + *(ULONG *)(buf + 1), MAXTYPELEN);
		memPtr += offset;
	}

	// Time to handle ptr and arrays	
	if (flags & 0x04)
	{
		if (sdl->vbi.skipPtrSteps == 0)
		{
			Strncat(sdl->vbi.encodedTypeString, "*", MAXTYPELEN);
			strins(sdl->vbi.ptrString, "*");
		}
			
		ptrDepth++;
	}

	if (sdl->vbi.encodedTypeString[9] == '*')
		memPtr = *(UBYTE **)memPtr; // was a pointer
		
	sdl->vbi.elementSize = 0;
	if (flags & 0x08) //continuation
		location = ParseContinuation(sdl, &ptrDepth, location, &sdl->vbi.elementSize);

    // And now for the actual types
	if (flags & 0x20) // ref to another typdef
	{
		if (decl != ONLYPARSE)
			ParseID(sdl, *(ULONG *)(buf + 5), level, POSTPONEDDECL,  memPtr);
	}
	else if (flags & 0x01) // ref to another tag)
	{
		if (decl != ONLYPARSE)
			ParseTag(sdl, *(ULONG *)(buf + 5),  level,  decl,  memPtr);
	}
	else if (flags & 0x02) // sub struct
	{
		location = ParseTag(sdl, location, level, decl, memPtr);
	}
	else
	{
		ULONG expand = FALSE;
		UBYTE bitFieldPos = 0;
		UBYTE bitFieldWidth = 0;
		ULONG hasChildren = (sdl->vbi.encodedTypeString[9] != '*'
		 || sdl->vbi.encodedTypeString[10] != 'F' ) &&
				!isValueOrPtrTo(sdl->vbi.encodedTypeString);		
		printBasicType(sdl, *(UBYTE *)(buf + 8));

		if (flags & 0x40) // bitfield
		{
			bitFieldPos = *(UBYTE *)(buf + 9);
			bitFieldWidth = *(UBYTE *)(buf + 10);
			SNPrintf(sdl->vbi.typeString, MAXTYPELEN, "%s:%ld", sdl->vbi.typeString, bitFieldWidth);
		}
			
		Strncat(sdl->vbi.typeString, sdl->vbi.ptrString, MAXTYPELEN);
		Strncat(sdl->vbi.typeString, sdl->vbi.funcPtrString, MAXTYPELEN);
		
		printBasicValue(sdl, *(UBYTE *)(buf + 8), memPtr, bitFieldPos, bitFieldWidth);

		if (*sdl->vbi.ptrString = '*')
			sdl->vbi.elementSize *= 4;
		else
			sdl->vbi.elementSize *= sizeOfBasicType(*(UBYTE *)(buf + 8));

		if (decl != ONLYPARSE)
			expand = createOrUpdateEntry(level, &sdl->vbi, hasChildren, memPtr);
		
		if (expand)
		{
			TEXT encodedType[sizeof(sdl->vbi.encodedTypeString)];
			
			Strncpy(encodedType, sdl->vbi.encodedTypeString, sizeof(encodedType));
			clearEntryInfo(sdl);
			ExpandArray(sdl, encodedType, level+1, memPtr,
				 sdl->vbi.elementSize, sdl->vbi.skipPtrSteps);
		}
	}
	
	clearEntryInfo(sdl);

	return location;
}

UBYTE *calcActualMemPtr(struct SourceDumpInfo *sdl, ULONG level,
		ULONG *attribPtr, UBYTE *memPtr)
{
	ULONG attrib = attribPtr[0];
	LONG offset = attribPtr[1];
	STRPTR name = sdl->vbi.nameString;

	if (0x00040000 & attrib)
		return memPtr;

	sdl->IsAReg = FALSE;

	if (!level)
	{
		if (0x01000000 & attrib)
		{
			// global or global static
			// memory can be near or far;
			if (0x00200000 & attrib)
			{
				memPtr = (UBYTE *)brokenRegs->a4;
				if (0x80000000 & attrib)
					memPtr += sdl->uninitializedNearOffset + offset;
				else
					memPtr +=  sdl->initializedNearOffset + offset;
			}
			else if (0x00800000 & attrib)
			{
				memPtr = sdl->dataMem[sdl->chipHunk];
				memPtr += sdl->chipOffset + offset;
			}
			else
			{
				if (0x80000000 & attrib) 
					memPtr = sdl->dataMem[sdl->uninitializedFarHunk]
						 + sdl->uninitializedFarOffset + offset;
				else
					memPtr = sdl->dataMem[sdl->initializedFarHunk]
						 + sdl->initializedFarOffset + offset;
			}				
		} else if (0x02000000 & attrib)
		{
			UBYTE actualName[256];
			struct SymbolNode *symNode;
			struct List *symbolList = (struct List *)memPtr;
	
			SNPrintf(actualName, sizeof(actualName), "_%s", name);
			symNode = (struct SymbolNode *)FindName(symbolList, actualName);
	
			if (symNode)
				memPtr = symNode->memPtr;
			else
				memPtr = 0;

		}
		else if (0x20000000 & attrib)
		{
			if (offset < 8)
				SNPrintf(name, MAXTYPELEN, "%s [D%ld]", name, offset);
			else if (offset < 16)
				SNPrintf(name, MAXTYPELEN, "%s [A%ld]", name, offset-8);
			else
				SNPrintf(name, MAXTYPELEN, "%s [FP%ld]", name, offset-16);
			
			offset *= sizeof(ULONG);
	
			// Variables smaller than 4 bytes are offset further
			sdl->IsAReg = TRUE;

			memPtr =  ((UBYTE*)brokenRegs) + offset;
		}
		else if (offset >= 0)
		{
			memPtr = (void *)~0; // optimized away
		}
		else
			memPtr =  memPtr + offset;
	}
	
	if (sdl->vbi.encodedTypeString[9] == '*')
	{
		sdl->IsAReg = FALSE;
		memPtr = *(UBYTE **)memPtr; // was a pointer
	}
	
	return memPtr;
}

ULONG ParseID(struct SourceDumpInfo *sdl, ULONG location,
	ULONG level,
	enum DecendMode decl, UBYTE *memPtr)
{
	UBYTE *buf = sdl->locationZero + location;
	ULONG flags = *buf;
	ULONG ptrDepth = 0;
	
	//kprintf("%4ld [%lx] ID\n",location, *buf);
	
	if (! *sdl->vbi.encodedTypeString)
		SNPrintf(sdl->vbi.encodedTypeString, MAXTYPELEN, "I%08lx", location);
	location += 13;
	if (flags & 0x44) // ref to another typdef or tag so we are longer
		location += 4;

	if (decl == DECL)
	{
		if (! *sdl->vbi.nameString)
			Strncpy(sdl->vbi.nameString, sdl->stringTable + *(ULONG *)(buf + 1), MAXTYPELEN);
	}
	else
		if (!sdl->handledFirstTypedef)
		{
			SNPrintf(sdl->vbi.typeString, MAXTYPELEN, "%s ", sdl->stringTable + *(ULONG *)(buf + 1));
			Strncat(sdl->vbi.typeString, sdl->vbi.ptrString, MAXTYPELEN);
			Strncat(sdl->vbi.typeString, sdl->vbi.funcPtrString, MAXTYPELEN);
			sdl->handledFirstTypedef = TRUE;
		}
		
	if (flags & 0x10)
	{
		if (sdl->vbi.skipPtrSteps == 0)
		{
			if( !sdl->handledFirstTypedef)
				strins(sdl->vbi.ptrString, "*");
			Strncat(sdl->vbi.encodedTypeString, "*", MAXTYPELEN);
		}
		ptrDepth++;
	}
	
	if (decl == DECL)
		sdl->vbi.elementSize = 0;
	if (flags & 0x20) //continuation
		location = ParseContinuation(sdl, &ptrDepth, location,
		 &sdl->vbi.elementSize);

	if (flags & 0x40) // ref to another typdef
	{
		memPtr = calcActualMemPtr(sdl, level, (ULONG *)(buf + 9), memPtr);
		if (sdl->vbi.skipPtrSteps >= ptrDepth)
			sdl->vbi.skipPtrSteps -= ptrDepth;
		ParseID(sdl, *(ULONG *)(buf + 5), level, POSTPONEDDECL, memPtr);
	}
	else if (flags & 0x04) // ref to another tag
	{
		memPtr = calcActualMemPtr(sdl, level, (ULONG *)(buf + 9), memPtr);
			
		ParseTag(sdl, *(ULONG *)(buf + 5),  level,  decl,  memPtr);
	}
	else if (flags & 0x08)
	{
		memPtr = calcActualMemPtr(sdl, level, (ULONG *)(buf + 5), memPtr);
			
		location = ParseTag(sdl, location, level, decl, memPtr);
	}
	else
	{
		ULONG expand = FALSE;
		ULONG hasChildren = (sdl->vbi.encodedTypeString[9] != '*'
		 || sdl->vbi.encodedTypeString[10] != 'F' ) &&
				!isValueOrPtrTo(sdl->vbi.encodedTypeString);
			
		if (!sdl->handledFirstTypedef)
		{
			printBasicType(sdl, *(UBYTE *)(buf + 8));
			Strncat(sdl->vbi.typeString, sdl->vbi.ptrString, MAXTYPELEN);
			Strncat(sdl->vbi.typeString, sdl->vbi.funcPtrString, MAXTYPELEN);
		}
		memPtr = calcActualMemPtr(sdl, level, (ULONG *)(buf + 5), memPtr);

		printBasicValue(sdl, *(UBYTE *)(buf + 8), memPtr, 0, 0);
		
		if (*sdl->vbi.ptrString == '*')
			sdl->vbi.elementSize *= 4;
		else
			sdl->vbi.elementSize *= sizeOfBasicType(*(UBYTE *)(buf + 8));
		
		if (decl != ONLYPARSE && ! (0x00000400 & *(ULONG *)(buf + 5)))
			expand = createOrUpdateEntry(level, &sdl->vbi, hasChildren, memPtr);
		
		if (expand)
		{
			TEXT encodedType[sizeof(sdl->vbi.encodedTypeString)];
			
			Strncpy(encodedType, sdl->vbi.encodedTypeString, sizeof(encodedType));
			clearEntryInfo(sdl);
			ExpandArray(sdl, encodedType, level+1, memPtr,
				 sdl->vbi.elementSize, sdl->vbi.totalSkipPtrSteps);
		}
	}

	clearEntryInfo(sdl);

	return location;
}

ULONG ParseFUNC(struct SourceDumpInfo *sdl, ULONG location, UBYTE *memPtr)
{
	UBYTE *buf = sdl->locationZero + location;
	ULONG flags = *buf;
	ULONG seriesPresent = 0xF8 & *((UBYTE*)(buf+1));
	ULONG seriesAreDecl = 0xC8;
	
/*	kprintf("%4ld [%lx] FUNC\n",location, *buf);
	kprintf("name=%s\n", sdl->stringTable + *(ULONG *)(buf + 2));
	kprintf("attrib=%lx\n", *((ULONG*)(buf+6)));
	kprintf("hunk offset=%lx\n", *((ULONG*)(buf+10)));
	kprintf("allocation on stack=%lx\n", *((ULONG*)(buf+14)));
	kprintf("paramsize=%lx\n", *((ULONG*)(buf+18)));
	kprintf("regs=0x%lx\n", *((UWORD*)(buf+22)));
	kprintf("fregs=0x%lx\n", *((UWORD*)(buf+24)));
*/
	location += 26;
	
	if (0x01 & *((UBYTE*)(buf+1)))  // returns a struct
		location += 4;		// location takes up 4 bytes
	else
		if (flags & 0x02)  // returns a typedef
			location += 4;		// takes up 4 bytes

	while (seriesPresent)
	{
		if (seriesPresent & 0x01)
		{
			ULONG done = FALSE;
			while (! done)
			{
				done = (sdl->locationZero[location] & 0x02) == 0;
				location = ParseID(sdl, location, 0, seriesAreDecl & 1 ? DECL : ONLYPARSE, memPtr);
			}
		}
		seriesPresent >>= 1;
		seriesAreDecl >>= 1;
	}
	return location;
}

void ParseBLK(struct SourceDumpInfo *sdl, ULONG location
	, ULONG findThisBlock, UBYTE *memPtr)
{
	UBYTE *buf = sdl->locationZero + location;
	ULONG flags = *buf;
	ULONG done;
	ULONG autoLocation;
	ULONG siblingLocation;
	ULONG finalChildLocation;
	ULONG firstBlockNum;
	ULONG lastBlockNum;
	ULONG lookingForThis;

	//kprintf("%4ld [%lx] BLK\n",location, *buf);
	
	location += 1;
	if (flags & 0x17)
	{
		autoLocation = *(ULONG *)(sdl->locationZero +location);
		location += 4;
	}
	if (flags & 0x40)
	{
		finalChildLocation = *(ULONG *)(sdl->locationZero +location);
		location += 4;
	}
	if (flags & 0x20)
	{
		// This is the sibling link
		siblingLocation = *(ULONG *)(sdl->locationZero + location);
		location += 4;
	}
	firstBlockNum = *(UWORD *)(sdl->locationZero + location);
	location += 2;	
	lastBlockNum = *(UWORD *)(sdl->locationZero + location);
	location += 2;	
		
	lookingForThis = (findThisBlock <= lastBlockNum)
				&& (findThisBlock >= firstBlockNum);

	sdl->vbi.referenceBlockNum = firstBlockNum;

	if (lookingForThis && (flags & 0x10))
	{
		done = FALSE;
		if (flags & 0x01)
			while (! done)
			{
				done = (sdl->locationZero[autoLocation] & 0x02) == 0;
				autoLocation = ParseTag(sdl, autoLocation, 0, ONLYPARSE, NULL);
			}
		done = FALSE;
		if (flags & 0x02)
			while (! done)
			{
				done = (sdl->locationZero[autoLocation] & 0x02) == 0;
				autoLocation = ParseID(sdl, autoLocation, 0, ONLYPARSE, NULL);
			}
		done = FALSE;
		if (flags & 0x04)
			while (! done)
			{
			//FIXME how about statics in blocks
				done = (sdl->locationZero[autoLocation] & 0x02) == 0;
				Strncpy(sdl->vbi.typeString, "static ", MAXTYPELEN);
				autoLocation = ParseID(sdl, autoLocation, 0, DECL, memPtr);
			}
		done = FALSE;
		while (! done)
		{
			done = (sdl->locationZero[autoLocation] & 0x02) == 0;
			autoLocation = ParseID(sdl, autoLocation, 0, DECL, memPtr);
		}
	}

	if ((findThisBlock < firstBlockNum) && (flags & 0x20))
		ParseBLK(sdl, siblingLocation, findThisBlock, memPtr);

	if (lookingForThis && (flags & 0x40))
		ParseBLK(sdl, finalChildLocation, findThisBlock, memPtr);
}

ULONG ParseFMAP(struct SourceDumpInfo *sdl, ULONG location
	, ULONG findThisBlock, UBYTE *memPtr)
{
	UBYTE *buf = sdl->locationZero + location;
	ULONG flags = *buf;
	ULONG finalBLKLocation;
	ULONG firstBlockNum;
	ULONG lastBlockNum;
	//kprintf("%4ld [%lx] FMAP\n",location, *buf);

	location += 5;
	
	finalBLKLocation = *(ULONG *)(sdl->locationZero + location);
	
	if (flags & 0x80)
		location += 4;

	firstBlockNum = *(UWORD *)(sdl->locationZero + location);
	location += 2;	
	if (flags & 0x40)
	{
		lastBlockNum = *(UWORD *)(sdl->locationZero + location);
		location += 2;
	}
	else
		lastBlockNum = firstBlockNum + (flags & 0x3F);
	
	if (findThisBlock < firstBlockNum)
		return location;
	if (findThisBlock > lastBlockNum)
		return location;
		
	sdl->vbi.referenceBlockNum = firstBlockNum;
	
	ParseFUNC(sdl, *(ULONG *)(buf + 1), memPtr);

	if (flags & 0x80)
		ParseBLK(sdl, finalBLKLocation, findThisBlock, memPtr);

	return location;
}

STRPTR LookupFunctionName(struct SourceDumpInfo *sdl, ULONG *location, ULONG findThisBlock)
{
	UBYTE *buf = sdl->locationZero + *location;
	ULONG flags = *buf;
	ULONG firstBlockNum;
	ULONG lastBlockNum;
	ULONG locationFUNC =  *(ULONG *)(sdl->locationZero + *location + 1);
	
	*location += 5;
	
	if (flags & 0x80)
		*location += 4;

	firstBlockNum = *(UWORD *)(sdl->locationZero + *location);
	*location += 2;
	if (flags & 0x40)
	{
		lastBlockNum = *(UWORD *)(sdl->locationZero + *location);
		*location += 2;
	}
	else
		lastBlockNum = firstBlockNum + (flags & 0x3F);

	if (findThisBlock < firstBlockNum)
		return NULL;
	if (findThisBlock > lastBlockNum)
		return NULL;
		
	return sdl->stringTable + *(ULONG *)(sdl->locationZero + locationFUNC + 2);
}

ULONG LookupFunctionStackUsage(struct SourceDumpInfo *sdl, ULONG *location, ULONG findThisBlock, ULONG *argSize)
{
	UBYTE *buf = sdl->locationZero + *location;
	ULONG flags = *buf;
	ULONG firstBlockNum;
	ULONG lastBlockNum;
	ULONG locationFUNC =  *(ULONG *)(sdl->locationZero + *location + 1);
	ULONG extra = 0;
	
	*location += 5;
	
	if (flags & 0x80)
		*location += 4;

	firstBlockNum = *(UWORD *)(sdl->locationZero + *location);
	*location += 2;
	if (flags & 0x40)
	{
		lastBlockNum = *(UWORD *)(sdl->locationZero + *location);
		*location += 2;
	}
	else
		lastBlockNum = firstBlockNum + (flags & 0x3F);

	if (findThisBlock < firstBlockNum)
		return ~0;
	if (findThisBlock > lastBlockNum)
		return ~0;

	// if FUNC has  extra field set the locations change
	if (*(sdl->locationZero + locationFUNC) & 0x02)
		extra = 4;
	else
		if (*(sdl->locationZero + locationFUNC+1) & 0x01)
			extra = 4;
		 
	*argSize = *(ULONG *)(sdl->locationZero + locationFUNC + 18 + extra);
	return *(ULONG *)(sdl->locationZero + locationFUNC + 14 + extra);
}

	
ULONG ParseSRC6(struct Debugger *dbgr, BPTR file, ULONG totalLen)
{
	struct SourceDumpInfo *sdl;
	
	sdl = AllocVec(sizeof(struct SourceDumpInfo), MEMF_ANY);
	if (! sdl)
		return totalLen; // we havn't read anything
	

	sdl->buffer = AllocVec(totalLen*4, MEMF_ANY);
	if (! sdl->buffer)
	{
		FreeVec(sdl);
		return totalLen; // we havn't read anything
	}
		
	FRead(file, sdl->buffer, totalLen*4, 1);
	
	ParseHeader(sdl);
	
	ParseGIH(sdl);
	
	sdl->node.ln_Name = sdl->filename;

	AddTail(&dbgr->sdlList, (struct Node *)sdl);

	return 0;
}

void FreeSRC6(struct Debugger *dbgr)
{
	struct SourceDumpInfo *sdl;
		
	while (sdl = (struct SourceDumpInfo *)RemTail(&dbgr->sdlList))
	{
		FreeVec(sdl->buffer);
		FreeVec(sdl);
	}
}

void UpdateVariableBrowser(struct Debugger *dbgr, STRPTR fullPath, ULONG blockNum, UBYTE *sp)
{
	struct Codecraft *ads = dbgr->ads;
	struct SourceDumpInfo *sdl;
	ULONG done = FALSE;
	ULONG location;
	ULONG i;

	if (fullPath)
	{
		// we are called with a zerobased blocknum
		// but our data is 1 based
		blockNum++;
					
		sdl = (struct SourceDumpInfo *)FindName(&dbgr->sdlList, fullPath);
	
		if (sdl)
		{
		   	ads->nextVariableNode = ads->variableBrowserList.lh_Head;
		
			clearEntryInfo(sdl);
			sdl->vbi.skipPtrSteps = 0;
			sdl->vbi.totalSkipPtrSteps = 0;
			sdl->vbi.referenceBlockNum = 0;
			
			if (sdl->ExtLocation)
			{
				location = sdl->ExtLocation;
				done = FALSE;
				while (! done)
				{
					done = (sdl->locationZero[location] & 0x02) == 0;
					location = ParseID(sdl, location, 0, DECL, (UBYTE *)&dbgr->symbolList);
				}
			}
		
			if (sdl->StaticLocation)
			{
				location = sdl->StaticLocation;
				done = FALSE;
				while (! done)
				{
					done = (sdl->locationZero[location] & 0x02) == 0;
					Strncpy(sdl->vbi.typeString, "static ", MAXTYPELEN);
					location = ParseID(sdl, location, 0, DECL, NULL);
				}
			}
			
			i = sdl->NumFuncs;
			location = sdl->FMAPLocation;
			while (i--)
				location = ParseFMAP(sdl, location, blockNum, sp);
		}
	}
		
	clearTrailingVariableNodes(ads);
}

void ExpandVariableBrowser(struct Debugger *dbgr, STRPTR fullPath, ULONG blockNum,
		 STRPTR encodedType, ULONG level, ULONG elementSize,
		 ULONG skipPtrSteps, ULONG refBlockNum, 
		 UBYTE *memPtr)
{
	struct SourceDumpInfo *sdl;

	// we are called with a zerobased blocknum
	// but our data is 1 based
	blockNum++;
				
	sdl = (struct SourceDumpInfo *)FindName(&dbgr->sdlList, fullPath);

	// The sdl is known to be good as we have looked it up before
	// ads->nextVariableNode is already setup;

	clearEntryInfo(sdl);
	sdl->vbi.referenceBlockNum = refBlockNum;
	
	//encodedString is: 1 char + ULONG in hex + continuation
	if (encodedType[9])
	{
		// expandArray expects level of elements and not the array
		ExpandArray(sdl, encodedType, level+1,
		 memPtr, elementSize, skipPtrSteps);
	}
	else
	{
		ULONG location = (ULONG)strtoul(encodedType+1, NULL, 16);
		sdl->vbi.skipPtrSteps = 0;
		sdl->vbi.totalSkipPtrSteps = 0;

		Strncpy(sdl->vbi.encodedTypeString, encodedType, MAXTYPELEN);
		ParseTag(sdl, location, level, DECLBUTNOTTAGITSELF, memPtr);
	}
}

UBYTE *advanceStackPointer(struct Debugger *dbgr, UBYTE *sp, UWORD *refPC, ULONG *argSize)
{
	STRPTR filename;
	ULONG lineNum=0;
	ULONG stackUsage = ~0;
 
	filename = LookupSourceLine(dbgr, refPC, &lineNum, NULL);

	if (lineNum)
	{
		ULONG i;
		ULONG location;
		struct SourceDumpInfo *sdl;
		
		sdl = (struct SourceDumpInfo *)FindName(&dbgr->sdlList, filename);
		
		if (sdl)
		{
			i = sdl->NumFuncs;
			location = sdl->FMAPLocation;
	
			while (i-- && (stackUsage == ~0))
				stackUsage = LookupFunctionStackUsage(sdl, &location, lineNum + 1, argSize);
		}
		if (stackUsage != ~0)
			sp += stackUsage;
	}

	// Always add 4 bytes as we just saw a return address
	// on the stack
	return sp + 4;
}

void parseStackFrame(struct Debugger *dbgr, UBYTE *sp, UWORD *refPC)
{
	STRPTR funcName=NULL;
	STRPTR filename;
	ULONG lineNum=0;
	ULONG spOffset=0;
 
	filename = LookupSourceLine(dbgr, refPC, &lineNum, &spOffset);
	
	if (lineNum)
	{
		ULONG i;
		ULONG location;
		struct SourceDumpInfo *sdl;
		
		sdl = (struct SourceDumpInfo *)FindName(&dbgr->sdlList, filename);
		
		if (sdl)
		{
			i = sdl->NumFuncs;
			location = sdl->FMAPLocation;
	
			while (i-- && (funcName == NULL))
				funcName = LookupFunctionName(sdl, &location, lineNum + 1);
		}
		
		sp += spOffset;
		
		if (dbgr->immediateReturnAddress == 0)
			dbgr->immediateReturnAddress = refPC;
	}
	
		
	createCallStackEntry(sp, funcName, filename, lineNum);
}

UBYTE *UpdateCallStackBrowser(struct Codecraft *ads, UWORD *brokenAddress)
{
	struct Debugger *dbgr = ads->debugger;
	struct Process *process = dbgr->process;
	UBYTE *sp = -4 + (UBYTE *)(brokenRegs+1); // stack is after regs
										// -4 is due how sp works
	UBYTE *firstSp = 0;
	UWORD *tmpPC = brokenAddress;
	ULONG argSize = 0;
		
	clearCallStackBrowser(ads);

	sp = advanceStackPointer(dbgr, sp, tmpPC, &argSize);
	// Subtract 4 as that is added always and that
	// is not correct the first time around
	sp -= 4; 
	
	// clear it here so we know we don't have any leftovers
	dbgr->immediateReturnAddress = 0;

	while (sp <= (UBYTE *)process->pr_Task.tc_SPUpper)
	{
		UWORD *returnAddress = (UWORD *)(*(ULONG *)sp);
		UWORD *opcodeAddress;
		ULONG found = FALSE;
		
		sp++; // we need to increment for next time around
			  // Do it here so we can just do continue
		
		opcodeAddress = returnAddress - 1;
		if ((opcodeAddress < (UWORD*)0xf80000 || opcodeAddress > (UWORD*)0xfc0000)
		  && !TypeOfMem(opcodeAddress))
			continue;
			
		if (((0xFFF8 & *opcodeAddress) == 0x4E90) // JSR reg
			|| (((0xFF00 & *opcodeAddress) == 0x6100)
			 && ((0xFF & *opcodeAddress) != 0x00)
			 && ((0xFF & *opcodeAddress) != 0xFF))) //BSR UBYTE
			found = TRUE;

		if (!found)
		{
			opcodeAddress--;
			
			if (((0xFFF8 & *opcodeAddress) == 0x4EA8) // JSR UWORD(reg)
				|| ((0xFFF8 & *opcodeAddress) == 0x4EE0) // JSR UBYTE(reg,reg)
				|| (*opcodeAddress == 0x4EB8) // JSR UWORD
				|| (*opcodeAddress == 0x4EBA) // JSR UWORD(pc)
				|| (*opcodeAddress == 0x4EBB) // JSR UBYTE(pc,reg)
				|| (*opcodeAddress == 0x6100)) // BSR UWORD
				found = TRUE;
		}
		
		if (!found)
		{
			opcodeAddress--;
				
			if ((*opcodeAddress == 0x4EB9) // JSR ULONG
				|| (*opcodeAddress == 0x61FF)) // BSR ULONG
				found = TRUE;
		}
		
		if (found)
		{
			sp--; // undo what we did at the top and then..
			parseStackFrame(dbgr, sp, tmpPC);
			if (! firstSp)
			{
				firstSp = sp;
				// clear this so it is set next parseStackFrame
				dbgr->immediateReturnAddress = 0;
			}
		

			sp += argSize;
			argSize = 0;
			tmpPC = returnAddress;
			found = FALSE;
			sp = advanceStackPointer(dbgr, sp, tmpPC, &argSize);
		}
	}
	return firstSp;
}
