#include <intuition/classes.h>

#include <proto/utility.h>
#include <proto/intuition.h>
#include <proto/alib.h>

struct ClassData
{
	ULONG (*handleUpdate)(struct TagItem *taglist);
};

Class *ICCallbackClass;

ULONG __ASM__ dispatchIC(__REG__(a0, Class *cl), __REG__(a2, Object *o), __REG__(a1, Msg msg))
{
	struct ClassData *cd;
	APTR retval = NULL; 

	switch (msg->MethodID)
	{
		case OM_NEW:
			if (retval = (APTR)DoSuperMethodA(cl, o, msg))
			{
				struct TagItem *taglist;
				taglist = ((struct opSet *)msg)->ops_AttrList;
				
				cd = INST_DATA(cl, retval);
				cd->handleUpdate = (ULONG (*)())GetTagData(TAG_USER, NULL, taglist);
			}
			break;

		case OM_UPDATE:
			cd = INST_DATA(cl, o);
			{
				struct TagItem *taglist;
				taglist = ((struct opSet *)msg)->ops_AttrList;
				
				cd->handleUpdate(taglist);
			}
			break;
			
		default:
			retval = (APTR)DoSuperMethodA(cl, o, msg);
			break;
	}
	
	return (ULONG)retval;
}

void createICClass(void)
{
	ICCallbackClass = MakeClass("iccallback", "rootclass", NULL, sizeof(struct ClassData), NULL);

	ICCallbackClass->cl_Dispatcher.h_Entry = (ULONG (*)())dispatchIC;
}

void freeICClass(void)
{
	FreeClass(ICCallbackClass);
}
